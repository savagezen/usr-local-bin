#!/bin/sh
# pam only unmounts, but does not lock volume, so...
# set cron to run as root every x minutes
# to check if user is logged in and lock
# encrypted home volume if not logged in.

for u in $(ls /home)
  do
    echo $u | grep $(who | awk '{print $1}')
    if [ $? -eq 0 ]; then
      # user logged in
      exit 0
    else
      fuser -k /home/$u				# kill all processes using volume
      umount -R /home/$u			# unmount volume and children
      cryptsetup close /dev/mapper/home-$u	# lock volume
    fi
  done

# set in /etc/lightdm/lightdm.conf : session-cleanup-script
# light dm no respecting setting 2/10/23
